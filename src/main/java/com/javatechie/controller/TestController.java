package com.javatechie.controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;
import lombok.extern.slf4j.Slf4j;



@RestController
@Slf4j
public class TestController {
    @GetMapping("/test")
    public String testMethod() {
        log.error("Test error method");
        log.info("Test info method");
        return "success";
    }
}
