package com.javatechie.controller;

import com.javatechie.model.request.AuthRequest;
import com.javatechie.exception.BusinessException;
import com.javatechie.model.base.ResponseData;
import com.javatechie.service.LoginService;
import jakarta.servlet.http.HttpServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/authenticate")
public class AuthController {


    @Autowired
    private LoginService loginService;

    @PostMapping("/login")
    public ResponseEntity<ResponseData> authenticateAndGetToken(@RequestBody AuthRequest authRequest,
                                                                HttpServletRequest http)  throws BusinessException {
        return ResponseEntity.ok()
                .body(new ResponseData<>().success(loginService.login(authRequest),http));
    }
}
