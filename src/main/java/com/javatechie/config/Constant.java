package com.javatechie.config;

public class Constant {
    public static class Role {
        public final static String ADMIN = "ADMIN";
        public final static String USER = "USER";
        public final static String MANAGER = "MANAGER";
    }
}
