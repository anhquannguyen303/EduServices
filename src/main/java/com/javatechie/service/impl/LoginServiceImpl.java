package com.javatechie.service.impl;

import com.javatechie.model.request.AuthRequest;
import com.javatechie.entity.UserInfo;
import com.javatechie.enums.ErrorCode;
import com.javatechie.exception.BusinessException;
import com.javatechie.model.response.LoginResponse;
import com.javatechie.repository.UserInfoRepository;
import com.javatechie.service.JwtService;
import com.javatechie.service.LoginService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Optional;

@Service
@Slf4j
public class LoginServiceImpl implements LoginService {

    @Autowired
    private AuthenticationManager authenticationManager;
    @Autowired
    private JwtService jwtService;
    @Autowired
    private UserInfoRepository repository;
    @Override
    @Transactional(rollbackFor = {BusinessException.class})
    public LoginResponse login(AuthRequest authRequest)  throws BusinessException{
        log.error("login method");
        Optional<UserInfo> userInfo=repository.findByName(authRequest.getUsername());
        if (userInfo.isEmpty()) {
            throw new BusinessException(ErrorCode.USER_NOT_FOUND);
        }
        Authentication authentication = authenticationManager.authenticate(new UsernamePasswordAuthenticationToken(authRequest.getUsername(),
                authRequest.getPassword()));
        if (authentication.isAuthenticated()) {
            log.error("login method END");
            return new LoginResponse(jwtService.generateToken(authRequest.getUsername()));
        } else {
            throw new BusinessException(ErrorCode.USER_NOT_FOUND);
        }

    }
}
